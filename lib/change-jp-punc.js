'use babel';

import { CompositeDisposable } from 'atom';

export default {
  subscriptions: null,
  config:{
    "normalize-to-period":{
      type:'boolean',
      default:true
    },
    "use-ASCII-digits":{
      type:'boolean',
      default:true
    }
  },

  togglePuncs() {
    var editor = atom.workspace.getActiveTextEditor();
    var toPeriod = atom.config.get('normalize-to-period');
    var asciiDigits = atom.config.get('use-ASCII-digits');
    var text = editor.getText();
    var newText = text;
    if (toPeriod) {
      newText = newText.replace(/。/g, '．').replace(/、/g, '，');
    } else {
      newText = newText.replace(/、/g, '，').replace(/。/g, '．');
    }
    if (asciiDigits) {
      var from = '０';
      var to = '0';
      var i = 0;
      for(i = 0; i < 10; i++) {
        newText = newText.replace(new RegExp(from, "g"), to);
        to = String.fromCharCode(1 + to.charCodeAt(0));
        from = String.fromCharCode(1 + from.charCodeAt(0));
      }
    }
    editor.setText(newText);
    return null;
  },

  activate(state) {
    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'change-jp-punc:togglePuncs': () => this.togglePuncs()
    }));
  },

  deactivate() {
    this.subscriptions.dispose();
  },

};
